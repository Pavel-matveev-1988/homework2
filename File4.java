import java.util.Scanner;
import java.util.Arrays;

class Program3 {
	public static void main(String[] args) {
		
		int array[] = {10, 13, 11, 12, 2, 4};

		for (int i = 0; i < array.length; i++) {
			int min = array[i];
			int indexOfMin = i;
			
			for (int j = i; j < array.length; j++) {
				if (array[j] < min) {
					min = array[j];
					indexOfMin = j;
				}
			}

			array[indexOfMin] = array[i];
			array[i] = min;
		}

		System.out.println(Arrays.toString(array));

		Scanner scanner = new Scanner(System.in);
		int numberForSearch = scanner.nextInt();

		boolean isExists = false;

		int left = 0;
		int right = array.length - 1;
		int middle;

		while (left <= right) {
			middle = (right + left) / 2;

			if (array[middle] < numberForSearch) {
				left = middle + 1;
			} else if (array[middle] > numberForSearch) {
				right = middle - 1;
			} else {
				isExists = true;
				break;
			}
		}		

		if (isExists) {
			System.out.println("Is exists");
		} else {
			System.out.println("Not exists");
		}
	}
}
