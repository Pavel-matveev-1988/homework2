import java.util.Arrays;
 public class Program4 {
 	public static void main(String[] args) {
 		int[] array1 = {115, 3, 45, 567, 8};
 		boolean isSorted = false;
 		int b;
 		while (!isSorted) {
 			isSorted = true;
 			for (int i = 0; i < array1.length-1; i++){
 				if(array1[i] > array1[i+1]) {
 					isSorted = false;

 					b = array1[i];
 					array1[i] = array1[i+1];
 					array1[i+1] = b;
 				}
 			}

 		}
 		System.out.println(Arrays.toString(array1));
 	}
 }